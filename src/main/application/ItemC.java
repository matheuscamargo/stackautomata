package main.application;

import main.DFS;
import main.Node;
import main.Transition;

public class ItemC {
	
	private static Node q1 = new Node("q1", true);
	private static Node q2 = new Node("q2", false);
	private static Node q3 = new Node("q3", false);
	private static Node q4 = new Node("q4", true);
	
	public static void run() {		
		q1.addTransition(new Transition ("eps", "eps", "$",   q2));
		q2.addTransition(new Transition ("a",   "eps", "0",   q2));
		q2.addTransition(new Transition ("b",   "0",   "eps", q3));
		q3.addTransition(new Transition ("b",   "0",   "eps", q3));
		q3.addTransition(new Transition ("eps", "$",   "eps", q4));
		
		DFS dfs = new DFS(q1, "aba");
		if (dfs.run()) 	System.out.println("\nCadeia aceita!");
		else           System.out.println("\nCadeia rejeitada!");
	}	
	
	public static void main(String[] args) {
		ItemC.run();
	}

}
