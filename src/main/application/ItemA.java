package main.application;

import main.DFS;
import main.Node;
import main.Transition;

public class ItemA {
	
	private static Node q1 = new Node("q1", false);
	private static Node q2 = new Node("q2", false);
	private static Node q3 = new Node("q3", false);
	private static Node q4 = new Node("q4", true);
	private static Node q5 = new Node("q5", false);
	private static Node q6 = new Node("q6", false);
	private static Node q7 = new Node("q7", true);
	
	public static void run() {		
		q1.addTransition(new Transition ("eps", "eps", "$",   q2));
		q2.addTransition(new Transition ("a",   "eps", "a",   q2));
		q2.addTransition(new Transition ("eps", "eps", "eps", q3));
		q2.addTransition(new Transition ("eps", "eps", "eps", q5));
		q3.addTransition(new Transition ("b",   "a",   "eps", q3));
		q3.addTransition(new Transition ("eps", "$",   "eps", q4));
		q4.addTransition(new Transition ("c",   "eps", "eps", q4));
		q5.addTransition(new Transition ("b",   "eps", "eps", q5));
		q5.addTransition(new Transition ("eps", "eps", "eps", q6));
		q6.addTransition(new Transition ("c",   "a",   "eps", q6));
		q6.addTransition(new Transition ("eps", "$",   "eps", q7));
		
		DFS dfs = new DFS(q1, "aabcc");
		if (dfs.run()) 	System.out.println("\nCadeia aceita!");
		else           System.out.println("\nCadeia rejeitada!");
	}	
	
	public static void main(String[] args) {
		ItemA.run();
	}

}
