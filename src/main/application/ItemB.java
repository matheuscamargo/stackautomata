package main.application;

import main.DFS;
import main.Node;
import main.Transition;

public class ItemB {
	
	private static Node q1 = new Node("q1", true);
	private static Node q2 = new Node("q2", false);
	private static Node q3 = new Node("q3", false);
	private static Node q4 = new Node("q4", true);
	
	public static void run() {		
		q1.addTransition(new Transition ("eps", "eps", "$",   q2));
		q2.addTransition(new Transition ("eps", "eps", "eps", q3));
		q2.addTransition(new Transition ("0",   "eps", "0",   q2));
		q2.addTransition(new Transition ("1",   "eps", "1",   q2));
		q3.addTransition(new Transition ("eps", "$",   "eps", q4));
		q3.addTransition(new Transition ("0",   "0",   "eps", q3));
		q3.addTransition(new Transition ("1",   "1",   "eps", q3));
		
		DFS dfs = new DFS(q1, "000111");
		if (dfs.run()) 	System.out.println("\nCadeia aceita!");
		else           System.out.println("\nCadeia rejeitada!");
	}	
	
	public static void main(String[] args) {
		ItemB.run();
	}

}
