package main;

public class Transition {
	public final String tapeSymbol;
	public final String stackOutSymbol;
	public final String stackInSymbol;
	public final Node nextNode;
	
	public Transition(String tapeSymbol,
					    String stackOutSymbol,
					    String stackInSymbol,
					    Node nextNode) {
		this.tapeSymbol = tapeSymbol;
		this.stackOutSymbol = stackOutSymbol;
		this.stackInSymbol = stackInSymbol;
		this.nextNode = nextNode;
	}	
}
