package main;

import java.util.LinkedList;
import java.util.List;

public class Node {
	private final boolean finalNode;
	public final String state;
	private List<Transition> transitionList;
	
	public Node (String state, boolean finalNode) {
		this.state = state;
		this.finalNode = finalNode;
		transitionList = new LinkedList<>();
	}
	
	public void addTransition(Transition transition) {
		transitionList.add(transition);
	}
	
	public boolean isFinal() {
		return finalNode;
	}
	
	public List<Transition> getTransitionList() {
		return transitionList;
	}
}