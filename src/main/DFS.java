package main;

import java.util.Stack;

public class DFS {
	private int depth = 0;
	private Node node;
	private String tape;
	private Stack<String> stack;
	private boolean result;
	
	public DFS(Node node, String tape, Stack<String> stack, int depth) {
		this.node = node;
		this.stack = stack;		
		this.tape = tape;
		this.depth = depth;
		printNodeInfo();
	}
	
	public DFS(Node node, String tape) {
		this(node, tape, new Stack<String>(), 0);
	}

	@SuppressWarnings("unchecked")
	public boolean run() {		
		result = false;
		if (tape.isEmpty() && node.isFinal()) return true;
		for (Transition transition : node.getTransitionList()) {
			if (!result) {
				printTransitionInfo(transition);
				Stack<String> nextStack = (Stack<String>)stack.clone();
				String nextTape;
				
				if (transition.tapeSymbol.equals("eps")) nextTape = tape;
				else if (tape.isEmpty() ||
						  !transition.tapeSymbol.equals(String.valueOf(tape.charAt(0)))) continue;
				else nextTape = tape.substring(1);
				
				if (!transition.stackOutSymbol.equals("eps")) {
					if (transition.stackOutSymbol.equals(nextStack.peek())) {
						nextStack.pop();
					}
					else continue;
				}
				
				if (!transition.stackInSymbol.equals("eps")) {
					nextStack.push(transition.stackInSymbol);
				}
				
				DFS dfs = new DFS(transition.nextNode, nextTape, nextStack, depth + 1);
				if (dfs.run()) result = true;
			}
		}
		return result;
	}
	
	private void printNodeInfo() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			stringBuilder.append("     ");
		}
		stringBuilder.append("Estado: ");
		stringBuilder.append(node.state);
		stringBuilder.append("; Fita: ");
		stringBuilder.append(tape);
		stringBuilder.append("; Pilha: ");
		for (String symbol : stack) {
			stringBuilder.append(symbol);
			stringBuilder.append(" ");
		}
		System.out.println(stringBuilder.toString());
	}
	
	private void printTransitionInfo(Transition transition) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			stringBuilder.append("     ");
		}
		stringBuilder.append("Transicao: ");
		stringBuilder.append(transition.tapeSymbol);
		stringBuilder.append(",");
		stringBuilder.append(transition.stackOutSymbol);
		stringBuilder.append("/");
		stringBuilder.append(transition.stackInSymbol);
		stringBuilder.append(" -> ");
		stringBuilder.append(transition.nextNode.state);
		System.out.println(stringBuilder.toString());
	}

}
